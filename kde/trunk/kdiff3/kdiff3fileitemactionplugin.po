msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-05-05 00:21+0000\n"
"PO-Revision-Date: 2021-07-15 07:57\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kdiff3/kdiff3fileitemactionplugin.pot\n"
"X-Crowdin-File-ID: 9596\n"

#: kdiff3fileitemaction.cpp:80
#, kde-format
msgid "KDiff3..."
msgstr "KDiff3..."

#: kdiff3fileitemaction.cpp:105
#, kde-format
msgid "Compare with %1"
msgstr "与 %1 比较"

#: kdiff3fileitemaction.cpp:111
#, kde-format
msgid "Merge with %1"
msgstr "与 %1 合并"

#: kdiff3fileitemaction.cpp:117
#, kde-format
msgid "Save '%1' for later"
msgstr "保存“%1”以供日后使用"

#: kdiff3fileitemaction.cpp:123
#, kde-format
msgid "3-way merge with base"
msgstr "以被比较文件为基础进行三路合并"

#: kdiff3fileitemaction.cpp:130
#, kde-format
msgid "Compare with..."
msgstr "比较..."

#: kdiff3fileitemaction.cpp:143
#, kde-format
msgid "Clear list"
msgstr "清除列表"

#: kdiff3fileitemaction.cpp:151
#, kde-format
msgid "Compare"
msgstr "比较"

#: kdiff3fileitemaction.cpp:157
#, kde-format
msgid "3 way comparison"
msgstr "3 路比较"

#: kdiff3fileitemaction.cpp:161
#, kde-format
msgid "About KDiff3 menu plugin..."
msgstr "关于 KDiff3 菜单插件..."

#: kdiff3fileitemaction.cpp:275
#, kde-format
msgid "KDiff3 File Item Action Plugin: Copyright (C) 2011 Joachim Eibl\n"
msgstr "KDiff3 文件项目操作插件：版权所有 (C) 2011 Joachim Eibl\n"

#: kdiff3fileitemaction.cpp:276
#, kde-format
msgid "Using the context menu extension:\n"
"For simple comparison of two selected files choose \"Compare\".\n"
"If the other file is somewhere else \"Save\" the first file for later. It will appear in the \"Compare with...\" submenu. Then use \"Compare With\" on the second file.\n"
"For a 3-way merge first \"Save\" the base file, then the branch to merge and choose \"3-way merge with base\" on the other branch which will be used as destination.\n"
"Same also applies to folder comparison and merge."
msgstr "使用右键菜单拓展：\n"
"对于所选的两个文件进行简单比较，请选择“比较”。\n"
"如果其他文件在别处保存了第一个文件以备后用，它就会出现在“进行比较...”的子菜单中，点击“进行比较”第二个文件即可。\n"
"对于三路合并，请先保存被比较文件，再合并分支。最后在“以被比较文件为基础进行三路合并”中选择用作目标分支的另一个分支。\n"
"以上同样适用于文件夹比较和文件夹合并。"

#: kdiff3fileitemaction.cpp:284
#, kde-format
msgid "About KDiff3 File Item Action Plugin"
msgstr "关于 KDiff3 文件项目动作插件"

