msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-29 00:17+0000\n"
"PO-Revision-Date: 2021-07-17 10:29\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kdepim-addons/kmail_editorgrammar_plugins.pot\n"
"X-Crowdin-File-ID: 9614\n"

#: grammalecte/plugin/grammalecteinterface.cpp:76
#, kde-format
msgid "&Check Grammar (Grammalecte)"
msgstr "检查语法 (Grammalecte, &C)"

#: grammalecte/plugin/grammalecteplugin.cpp:39
#, kde-format
msgid "Grammalecte Plugin"
msgstr "Grammalecte 插件"

#: grammalecte/src/grammalecteconfigdialog.cpp:18
#, kde-format
msgctxt "@title:window"
msgid "Configure Grammalecte"
msgstr "配置 Grammalecte"

#: grammalecte/src/grammalecteconfigwidget.cpp:34
#, kde-format
msgid "General"
msgstr "常规"

#: grammalecte/src/grammalecteconfigwidget.cpp:35
#, kde-format
msgid "Grammar Settings"
msgstr "语法设置"

#: grammalecte/src/grammalecteconfigwidget.cpp:60
#, kde-format
msgid "Impossible to get options. Please verify that you have grammalected installed."
msgstr "无法获取选项。请确认您已经安装了 grammalected。"

#: grammalecte/src/grammalecteconfigwidget.cpp:61
#, kde-format
msgid "Error during Extracting Options"
msgstr "提取选项时出错"

#: grammalecte/src/grammalecteconfigwidget.cpp:110
#, kde-format
msgid "Press Button for Reloading Settings"
msgstr "可按按钮来重新加载设置"

#: grammalecte/src/grammalecteconfigwidget.cpp:117
#, kde-format
msgid "Reload Settings"
msgstr "重新加载设置"

#: grammalecte/src/grammalecteconfigwidget.cpp:134
#, kde-format
msgid "Python Path:"
msgstr "Python 路径："

#: grammalecte/src/grammalecteconfigwidget.cpp:138
#, kde-format
msgid "Add full 'grammalecte-cli.py' path"
msgstr "添加完整的“grammalecte-cli.py”的路径"

#: grammalecte/src/grammalecteconfigwidget.cpp:139
#, kde-format
msgid "Grammalecte Path:"
msgstr "Grammalecte 路径："

#: grammalecte/src/grammalecteresultwidget.cpp:56
#, kde-format
msgid "Python path is missing."
msgstr "Python 的路径不能为空。"

#: grammalecte/src/grammalecteresultwidget.cpp:60
#, kde-format
msgid "Grammalecte path not found."
msgstr "Grammalecte 的路径不能为空。"

#: grammalecte/src/grammalecteresultwidget.cpp:67
#, kde-format
msgid "Grammalecte program file not found."
msgstr "无法找到 Grammalecte 程序的文件。"

#: grammalecte/src/grammalecteresultwidget.cpp:71
#, kde-format
msgid "Grammalecte cli file not found."
msgstr "无法找到 Grammalecte 命令行的文件。"

#: grammalecte/src/grammalecteresultwidget.cpp:75
#, kde-format
msgid "Grammalecte error"
msgstr "Grammalecte 出错"

#: grammarcommon/grammarresulttextedit.cpp:37
#, kde-format
msgid "Any text to check."
msgstr "任何要检查的文本。"

#: grammarcommon/grammarresulttextedit.cpp:79
#, kde-format
msgid "See on: %1"
msgstr "参见：%1"

#: grammarcommon/grammarresulttextedit.cpp:111
#, kde-format
msgid "Replacement"
msgstr "替换"

#: grammarcommon/grammarresulttextedit.cpp:120
#, kde-format
msgid "Online Grammar Information"
msgstr "在线语法信息"

#: grammarcommon/grammarresulttextedit.cpp:133
#, kde-format
msgid "Check Again"
msgstr "再次检查"

#: grammarcommon/grammarresultwidget.cpp:31
#, kde-format
msgid "Close"
msgstr "关闭"

#: languagetool/plugin/languagetoolinterface.cpp:80
#, kde-format
msgid "&Check Grammar (LanguageTool)"
msgstr "检查语法 (LanguageTool, &C)"

#: languagetool/plugin/languagetoolinterface.cpp:94
#, kde-format
msgid "You do not use local instance.\n"
"Your text will send on a external web site (https://languagetool.org/).\n"
"Do you want to continue?"
msgstr "您没有使用本地实例。\n"
"您的文本将发送到外部网站 (https://languagetool.org/)。\n"
"您想要继续吗？"

#: languagetool/plugin/languagetoolinterface.cpp:95
#, kde-format
msgid "Check Grammar with LanguageTool"
msgstr "使用 LanguageTool 检查语法"

#: languagetool/plugin/languagetoolplugin.cpp:39
#, kde-format
msgid "LanguageTool Plugin"
msgstr "LanguageTool 插件"

#: languagetool/src/languagetoolcombobox.cpp:33
#, kde-format
msgid "English"
msgstr "英语"

#: languagetool/src/languagetoolcombobox.cpp:34
#, kde-format
msgid "Asturian"
msgstr "阿斯图里亚斯语"

#: languagetool/src/languagetoolcombobox.cpp:35
#, kde-format
msgid "Belarusian"
msgstr "白俄罗斯语"

#: languagetool/src/languagetoolcombobox.cpp:36
#, kde-format
msgid "Breton"
msgstr "布列塔尼语"

#: languagetool/src/languagetoolcombobox.cpp:37
#, kde-format
msgid "Catalan"
msgstr "加泰罗尼亚语"

#: languagetool/src/languagetoolcombobox.cpp:38
#, kde-format
msgid "Chinese"
msgstr "中文"

#: languagetool/src/languagetoolcombobox.cpp:39
#, kde-format
msgid "Danish"
msgstr "丹麦语"

#: languagetool/src/languagetoolcombobox.cpp:40
#, kde-format
msgid "Dutch"
msgstr "荷兰语"

#: languagetool/src/languagetoolcombobox.cpp:41
#, kde-format
msgid "English (Australian)"
msgstr "英语 (澳大利亚)"

#: languagetool/src/languagetoolcombobox.cpp:42
#, kde-format
msgid "English (Canadian)"
msgstr "英语 (加拿大)"

#: languagetool/src/languagetoolcombobox.cpp:43
#, kde-format
msgid "Esperanto"
msgstr "世界语"

#: languagetool/src/languagetoolcombobox.cpp:44
#, kde-format
msgid "French"
msgstr "法语"

#: languagetool/src/languagetoolcombobox.cpp:45
#, kde-format
msgid "Galician"
msgstr "加利西亚语"

#: languagetool/src/languagetoolcombobox.cpp:46
#, kde-format
msgid "German"
msgstr "德语"

#: languagetool/src/languagetoolcombobox.cpp:47
#, kde-format
msgid "Greek"
msgstr "希腊语"

#: languagetool/src/languagetoolcombobox.cpp:48
#, kde-format
msgid "Italian"
msgstr "意大利语"

#: languagetool/src/languagetoolcombobox.cpp:49
#, kde-format
msgid "Japanese"
msgstr "日语"

#: languagetool/src/languagetoolcombobox.cpp:50
#, kde-format
msgid "Khmer"
msgstr "高棉语"

#: languagetool/src/languagetoolcombobox.cpp:51
#, kde-format
msgid "Persian"
msgstr "波斯语"

#: languagetool/src/languagetoolcombobox.cpp:52
#, kde-format
msgid "Polish"
msgstr "波兰语"

#: languagetool/src/languagetoolcombobox.cpp:53
#, kde-format
msgid "Portuguese"
msgstr "葡萄牙语"

#: languagetool/src/languagetoolcombobox.cpp:54
#, kde-format
msgid "Romanian"
msgstr "罗马尼亚语"

#: languagetool/src/languagetoolcombobox.cpp:55
#, kde-format
msgid "Russian"
msgstr "俄语"

#: languagetool/src/languagetoolcombobox.cpp:56
#, kde-format
msgid "Slovak"
msgstr "斯洛伐克语"

#: languagetool/src/languagetoolcombobox.cpp:57
#, kde-format
msgid "Slovenian"
msgstr "斯洛文尼亚语"

#: languagetool/src/languagetoolcombobox.cpp:58
#, kde-format
msgid "Spanish"
msgstr "西班牙语"

#: languagetool/src/languagetoolcombobox.cpp:59
#, kde-format
msgid "Swedish"
msgstr "瑞典语"

#: languagetool/src/languagetoolcombobox.cpp:60
#, kde-format
msgid "Tagalog"
msgstr "他加禄语"

#: languagetool/src/languagetoolcombobox.cpp:61
#, kde-format
msgid "Tamil"
msgstr "泰米尔语"

#: languagetool/src/languagetoolcombobox.cpp:62
#, kde-format
msgid "Ukrainian"
msgstr "乌克兰语"

#: languagetool/src/languagetoolconfigdialog.cpp:23
#, kde-format
msgctxt "@title:window"
msgid "Configure LanguageTool"
msgstr "配置 LanguageTool"

#: languagetool/src/languagetoolconfigwidget.cpp:30
#, kde-format
msgid "Use Local Instance"
msgstr "使用本地实例"

#: languagetool/src/languagetoolconfigwidget.cpp:37
#, kde-format
msgid "Instance Path:"
msgstr "实例路径："

#: languagetool/src/languagetoolconfigwidget.cpp:53
#, kde-format
msgid "Language:"
msgstr "语言："

#: languagetool/src/languagetoolconfigwidget.cpp:66
#, kde-format
msgid "Refresh"
msgstr "刷新"

#: languagetool/src/languagetoolresultwidget.cpp:59
#, kde-format
msgid "An error was reported: %1"
msgstr "报告了一个错误：%1"

#: languagetool/src/languagetoolresultwidget.cpp:59
#, kde-format
msgid "Failed to check grammar."
msgstr "检查语法失败。"

#: languagetool/src/languagetoolupdatecombobox.cpp:68
#, kde-format
msgid "An error occurred attempting to load the list of available languages:\n"
"%1"
msgstr "尝试加载可用语言列表时出错：\n"
"%1"

#: languagetool/src/languagetoolupdatecombobox.cpp:68
#, kde-format
msgid "List of Languages"
msgstr "语言列表"

