msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:07+0200\n"
"PO-Revision-Date: 2021-08-23 13:43\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kipi-plugins/kipiplugin_flickr.pot\n"
"X-Crowdin-File-ID: 10454\n"

#: comboboxintermediate.h:54
#, kde-format
msgid "Various"
msgstr "各种"

#: flickrlist.cpp:420
#, kde-format
msgid "Check if photo should be publicly visible or use Upload Options tab to specify this for all images"
msgstr "勾选后，照片将被设为公开可见，您也可以使用“上传选项”选项卡添加标签到所有图像。"

#: flickrlist.cpp:423
#, kde-format
msgid "Check if photo should be visible to family or use Upload Options tab to specify this for all images"
msgstr "勾选后，照片将被设为家庭成员可见，您也可以使用“上传选项”选项卡添加标签到所有图像。"

#: flickrlist.cpp:426
#, kde-format
msgid "Check if photo should be visible to friends or use Upload Options tab to specify this for all images"
msgstr "勾选后，照片将被设为朋友可见，您也可以使用“上传选项”选项卡添加标签到所有图像。"

#: flickrlist.cpp:429
#, kde-format
msgid "Indicate the safety level for the photo or use Upload Options tab to specify this for all images"
msgstr "指定照片的安全级别，您也可以使用“上传选项”选项卡添加标签到所有图像。"

#: flickrlist.cpp:432
#, kde-format
msgid "Indicate what kind of image this is or use Upload Options tab to specify this for all images"
msgstr "指定图像的类型，您也可以使用“上传选项”选项卡添加标签到所有图像。"

#: flickrlist.cpp:445
#, kde-format
msgid "Add extra tags per image or use Upload Options tab to add tags for all images"
msgstr "您可以逐张添加额外标签到图像，也可以使用“上传选项”选项卡添加标签到所有图像"

#: flickrlist.cpp:462
#, kde-format
msgid "Enter extra tags, separated by commas."
msgstr "输入其它标记，以逗号分割。"

#: flickrtalker.cpp:265
#, kde-format
msgid "Getting the maximum allowed file size."
msgstr "正在获取文件大小限制信息"

#: flickrtalker.cpp:550
#, kde-format
msgid "File Size exceeds maximum allowed file size."
msgstr "文件大小超出了最大限制。"

#: flickrtalker.cpp:603
#, kde-format
msgid "No photo specified"
msgstr "未指定照片"

#: flickrtalker.cpp:607
#, kde-format
msgid "General upload failure"
msgstr "常规上传失败"

#: flickrtalker.cpp:611
#, kde-format
msgid "Filesize was zero"
msgstr "文件大小为零"

#: flickrtalker.cpp:615
#, kde-format
msgid "Filetype was not recognized"
msgstr "文件类型不可识别"

#: flickrtalker.cpp:619
#, kde-format
msgid "User exceeded upload limit"
msgstr "用户超出上传限制"

#: flickrtalker.cpp:623
#, kde-format
msgid "Invalid signature"
msgstr "无效的签名"

#: flickrtalker.cpp:627
#, kde-format
msgid "Missing signature"
msgstr "缺失的签名"

#: flickrtalker.cpp:631
#, kde-format
msgid "Login Failed / Invalid auth token"
msgstr "登录失败 / 无效的验证令牌"

#: flickrtalker.cpp:635
#, kde-format
msgid "Invalid API Key"
msgstr "无效的 API 密钥"

#: flickrtalker.cpp:639
#, kde-format
msgid "Service currently unavailable"
msgstr "服务当前不可用"

#: flickrtalker.cpp:643
#, kde-format
msgid "Invalid Frob"
msgstr "无效的 Frob(临时密钥)"

#: flickrtalker.cpp:647
#, kde-format
msgid "Format \"xxx\" not found"
msgstr "没有找到格式“xxx”"

#: flickrtalker.cpp:651
#, kde-format
msgid "Method \"xxx\" not found"
msgstr "没有找到方法“xxx”"

#: flickrtalker.cpp:655
#, kde-format
msgid "Invalid SOAP envelope"
msgstr "无效的 SOAP 信封"

#: flickrtalker.cpp:659
#, kde-format
msgid "Invalid XML-RPC Method Call"
msgstr "无效的 XML-PRC 方法调用"

#: flickrtalker.cpp:663
#, kde-format
msgid "The POST method is now required for all setters"
msgstr "POST 方法现在对所有设置者都需要"

#: flickrtalker.cpp:667
#, kde-format
msgid "Unknown error"
msgstr "未知错误"

#: flickrtalker.cpp:672 flickrtalker.cpp:696 flickrtalker.cpp:852
#: flickrwindow.cpp:416
#, kde-format
msgid "Error"
msgstr "错误"

#: flickrtalker.cpp:673
#, kde-format
msgid "Error Occurred: %1\n"
"Cannot proceed any further."
msgstr "发生错误：%1\n"
"无法进一步处理。"

#: flickrtalker.cpp:852
#, kde-format
msgid "PhotoSet creation failed: "
msgstr "照片专辑创建失败："

#: flickrtalker.cpp:947
#, kde-format
msgid "Failed to fetch list of photo sets."
msgstr "获取照片专辑列表失败。"

#: flickrtalker.cpp:1027
#, kde-format
msgid "Failed to upload photo"
msgstr "上传照片失败"

#: flickrtalker.cpp:1086
#, kde-format
msgid "Failed to query photo information"
msgstr "查询照片信息失败"

#: flickrwidget.cpp:63
#, kde-format
msgid "Remove Account"
msgstr "删除账户"

#: flickrwidget.cpp:73 flickrwidget.cpp:77
#, kde-format
msgctxt "photo permissions"
msgid "Public"
msgstr "公开"

#: flickrwidget.cpp:76
#, kde-format
msgid "This is the list of images to upload to your Flickr account."
msgstr "这是一份要上传到您 Flickr 账户的图像清单。"

#: flickrwidget.cpp:81
#, kde-format
msgid "Extra tags"
msgstr "其它标记"

#: flickrwidget.cpp:87 flickrwidget.cpp:98
#, kde-format
msgctxt "photo permissions"
msgid "Family"
msgstr "家庭"

#: flickrwidget.cpp:92 flickrwidget.cpp:100
#, kde-format
msgctxt "photo permissions"
msgid "Friends"
msgstr "朋友"

#: flickrwidget.cpp:107
#, kde-format
msgid "Safety level"
msgstr "安全等级"

#: flickrwidget.cpp:109
#, kde-format
msgid "Content type"
msgstr "内容类型"

#: flickrwidget.cpp:112
#, kde-format
msgctxt "photo safety level"
msgid "Safe"
msgstr "安全"

#: flickrwidget.cpp:113
#, kde-format
msgctxt "photo safety level"
msgid "Moderate"
msgstr "适度"

#: flickrwidget.cpp:114
#, kde-format
msgctxt "photo safety level"
msgid "Restricted"
msgstr "限制"

#: flickrwidget.cpp:115 flickrwidget.cpp:211
#, kde-format
msgctxt "photo content type"
msgid "Photo"
msgstr "照片"

#: flickrwidget.cpp:116 flickrwidget.cpp:212
#, kde-format
msgctxt "photo content type"
msgid "Screenshot"
msgstr "截图"

#: flickrwidget.cpp:117 flickrwidget.cpp:213
#, kde-format
msgctxt "photo content type"
msgid "Other"
msgstr "其它"

#: flickrwidget.cpp:134
#, kde-format
msgid "Tag options"
msgstr "标记选项"

#: flickrwidget.cpp:138
#, kde-format
msgid "Use Host Application Tags"
msgstr "使用主应用程序标记"

#: flickrwidget.cpp:140 flickrwidget.cpp:501
#, kde-format
msgid "More tag options"
msgstr "更多标记选项"

#: flickrwidget.cpp:154
#, kde-format
msgid "Added Tags: "
msgstr "添加的标记："

#: flickrwidget.cpp:156
#, kde-format
msgid "Enter new tags here, separated by commas."
msgstr "在此输入新的标记，以逗号分割。"

#: flickrwidget.cpp:158
#, kde-format
msgid "Add tags per image"
msgstr "为每张图像添加标记"

#: flickrwidget.cpp:159
#, kde-format
msgid "If checked, you can set extra tags for each image in the File List tab"
msgstr "如果选中，您可以为文件列表标签中的每张图像设定额外的标记"

#: flickrwidget.cpp:163
#, kde-format
msgid "Strip Spaces From Tags"
msgstr "从标记中去除空格"

#: flickrwidget.cpp:176
#, kde-format
msgid "Publication Options"
msgstr "发布选项"

#: flickrwidget.cpp:181
#, kde-format
msgctxt "As in accessible for people"
msgid "Public (anyone can see them)"
msgstr "公开(任何人可以看到它们)"

#: flickrwidget.cpp:184
#, kde-format
msgid "Visible to Family"
msgstr "对家人可见"

#: flickrwidget.cpp:187
#, kde-format
msgid "Visible to Friends"
msgstr "对朋友可见"

#: flickrwidget.cpp:190 flickrwidget.cpp:488
#, kde-format
msgid "More publication options"
msgstr "更多发布选项"

#: flickrwidget.cpp:203
#, kde-format
msgid "Safety level:"
msgstr "安全等级："

#: flickrwidget.cpp:205
#, kde-format
msgid "Safe"
msgstr "安全"

#: flickrwidget.cpp:206
#, kde-format
msgid "Moderate"
msgstr "适度"

#: flickrwidget.cpp:207
#, kde-format
msgid "Restricted"
msgstr "限制"

#: flickrwidget.cpp:209
#, kde-format
msgid "Content type:"
msgstr "内容类型："

#: flickrwidget.cpp:304
#, kde-format
msgid "<b><h2><a href='http://www.23hq.com'><font color=\"#7CD164\">23</font></a> Export</h2></b>"
msgstr "<b><h2><a href='http://www.23hq.com'><font color=\"#7CD164\">23</font></a> 导出</h2></b>"

#: flickrwidget.cpp:311
#, kde-format
msgid "<b><h2><a href='http://www.flickr.com'><font color=\"#0065DE\">flick</font><font color=\"#FF0084\">r</font></a> Export</h2></b>"
msgstr "<b><h2><a href='http://www.flickr.com'><font color=\"#0065DE\">flick</font><font color=\"#FF0084\">r</font></a> 导出</h2></b>"

#: flickrwidget.cpp:484
#, kde-format
msgid "Fewer publication options"
msgstr "更少发布选项"

#: flickrwidget.cpp:506
#, kde-format
msgid "Fewer tag options"
msgstr "更少的标签选项"

#: flickrwindow.cpp:71
#, kde-format
msgid "Export to %1 Web Service"
msgstr "导出至 %1 网络服务"

#: flickrwindow.cpp:119
#, kde-format
msgid "Start Uploading"
msgstr "开始上传"

#: flickrwindow.cpp:131
#, kde-format
msgid "Flickr/23 Export"
msgstr "Flickr/23 导出"

#: flickrwindow.cpp:132
#, kde-format
msgid "A tool to export an image collection to a Flickr / 23 web service."
msgstr "一个将图片集合导出至 Flickr / 23 网络服务的工具。"

#: flickrwindow.cpp:134
#, kde-format
msgid "(c) 2005-2008, Vardhman Jain\n"
"(c) 2008-2015, Gilles Caulier\n"
"(c) 2009, Luka Renko\n"
"(c) 2015, Shourya Singh Gupta"
msgstr "(c) 2005-2008，Vardhman Jain\n"
"(c) 2008-2009，Gilles Caulier\n"
"(c) 2009，Luka Renko"

#: flickrwindow.cpp:139
#, kde-format
msgid "Vardhman Jain"
msgstr "Vardhman Jain"

#: flickrwindow.cpp:139
#, kde-format
msgid "Author and maintainer"
msgstr "作者和维护者"

#: flickrwindow.cpp:142
#, kde-format
msgid "Gilles Caulier"
msgstr "Gilles Caulier"

#: flickrwindow.cpp:142 flickrwindow.cpp:145
#, kde-format
msgid "Developer"
msgstr "开发者"

#: flickrwindow.cpp:145
#, kde-format
msgid "Shourya Singh Gupta"
msgstr "Shourya Singh Gupta"

#: flickrwindow.cpp:557
#, kde-format
msgid "Photostream Only"
msgstr "仅照片数据流"

#: flickrwindow.cpp:741
#, kde-format
msgid "Flickr Export"
msgstr "Flickr 导出"

#: flickrwindow.cpp:759
#, kde-format
msgid "Failed to Fetch Photoset information from %1. %2\n"
msgstr "从 %1 获取照片专辑信息失败。%2\n"

#: flickrwindow.cpp:765
#, kde-format
msgid "Warning"
msgstr "警告"

#: flickrwindow.cpp:766
#, kde-format
msgid "Failed to upload photo into %1. %2\n"
"Do you want to continue?"
msgstr "上传照片到 %1 失败。%2\n"
"您想要继续吗？"

#: flickrwindow.cpp:769
#, kde-format
msgid "Continue"
msgstr "继续"

#: flickrwindow.cpp:770
#, kde-format
msgid "Cancel"
msgstr "取消"

#. i18n: ectx: Menu (Export)
#: kipiplugin_flickrui.rc:7
#, kde-format
msgid "&Export"
msgstr "导出(&E)"

#. i18n: ectx: ToolBar (mainToolBar)
#: kipiplugin_flickrui.rc:14
#, kde-format
msgid "Main Toolbar"
msgstr "主工具栏"

#: plugin_flickr.cpp:100
#, kde-format
msgid "Export to Flick&r..."
msgstr "导出至 Flick&r..."

#: selectuserdlg.cpp:47
#, kde-format
msgid "Flickr Account Selector"
msgstr "Flickr 账户选择器"

#: selectuserdlg.cpp:52
#, kde-format
msgid "Add another account"
msgstr "添加其它账户"

#: selectuserdlg.cpp:75
#, kde-format
msgid "Choose the %1 account to use for exporting images:"
msgstr "选择用于导出图像的 %1 账户："

